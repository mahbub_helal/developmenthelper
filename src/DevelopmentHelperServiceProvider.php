<?php

namespace MahbubHelal\DevelopmentHelper;

use Doctrine\SqlFormatter\NullHighlighter;
use Doctrine\SqlFormatter\SqlFormatter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\ServiceProvider;

class DevelopmentHelperServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        if (env('APP_DEBUG')) {
            $new = true;

            DB::listen(function ($query) use (&$new) {
                if ($new) {
                    File::delete(storage_path('/logs/query.log'));

                    $new = false;
                }

                $bindings = $query->bindings;

                $sql = preg_replace_callback(
                    '/([\s\(])(\?)([\s,\)]|$)/',
                    function ($matches) use (&$bindings) {
                        $binding = array_shift($bindings);

                        if (is_string($binding)) {
                            $binding = "'" . $binding . "'";
                        }

                        return $matches[1] . $binding . $matches[3];
                    },
                    $query->sql
                );

                $sql = (new SqlFormatter(new NullHighlighter()))->format($sql);

                File::append(
                    storage_path('/logs/query.log'),
                    $sql . PHP_EOL . PHP_EOL
                );
            });

            $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        }
    }
}
