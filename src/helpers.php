<?php

use Illuminate\Support\Facades\Log;

function measure($label = 'default', $dump = true, $log = true)
{
    static $times = [];

    if (in_array($label, array_keys($times))) {
        $total = microtime(true) - $times[$label];

        $message = $label . ' took ' . $total;

        if ($log) {
            config([
                'logging.channels.timings' => [
                    'driver' => 'single',
                    'path'   => storage_path('logs/timings.log')
                ]
            ]);

            Log::channel('timings')->info($message);
        }

        if ($dump) {
            dump($message);
        }

        unset($times[$label]);

        return $total;
    } else {
        $times[$label] = microtime(true);
    }
}
