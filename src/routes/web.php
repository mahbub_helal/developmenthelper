<?php

use Doctrine\SqlFormatter\SqlFormatter;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

Route::get('/query', function () {
    $data = File::get(storage_path('/logs/query.log'));

    $queries = explode("\n\n", $data);

    echo '<hr>';

    foreach ($queries as $query) {
        if ($query) {
            echo (new SqlFormatter())->format($query) . '<hr>';
        }
    }
});
